//
//  FilmDetailViewController.swift
//  apupyshevIMDBmkTest
//
//  Created by Alexey Pupyshev on 02/09/15.
//  Copyright (c) 2015 Alexey Pupyshev. All rights reserved.
//

import UIKit

class FilmDetailViewController: UIViewController {
    
    // SearchedResultTableViewController - 0
    // BookmarkedResultsTableViewController - 1
    
    var flag = 0
    var film: Film = Film()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var directorLabel: UILabel!
    @IBOutlet weak var writersLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var posterView: UIImageView!
    @IBOutlet weak var bookmarkedFilmImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = film.title

        titleLabel.text = film.title
        infoLabel.text = "\(film.runtime) - \(film.genre) - \(film.released)"
        ratingLabel.text = "Rating: " + film.imdbRating
        directorLabel.text = "Director: " + film.director
        writersLabel.text = "Writers: " + film.writer
        typeLabel.text = "Type: " + film.type
        descriptionLabel.text = film.plot
        posterView.image = UIImage(data: film.poster)
    }

    @IBAction func actionButton(sender: UIBarButtonItem) {
        var titleA = ""
        
        if flag == 0 {
            titleA = "Add to bookmarks"

        } else {
            titleA = "Remove from bookmarks"

        }
        
        let saveToCoreData = SaveToCoreDataActivity(title: titleA) { () -> () in
            if self.flag == 0 {
                var refreshAlert = UIAlertController(title: "Add", message: "Add this to bookmarks?", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Add", style: .Default, handler: { (action: UIAlertAction!) in
                    self.film.addedDate = "Time there"
                    self.film.saveManaged()
                    NSNotificationCenter.defaultCenter().postNotificationName("refreshTableView", object: nil)
                }))
                
                refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in }))
                
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            } else {
                var refreshAlert = UIAlertController(title: "Delete", message: "Delete this from bookmarks?", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Delete", style: .Default, handler: { (action: UIAlertAction!) in
                    self.film.removeManaged()
                    NSNotificationCenter.defaultCenter().postNotificationName("refreshTableView", object: nil)
                }))
                
                refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in }))
                
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            }
        }
        
        var activityView = UIActivityViewController(activityItems: [self.film.title, NSURL(string: "http://www.imdb.com/title/" + self.film.imdbID)!, UIImage(data: film.poster)!], applicationActivities: [saveToCoreData])
        activityView.excludedActivityTypes = [UIActivityTypeSaveToCameraRoll,
                UIActivityTypeAssignToContact,
                UIActivityTypeAirDrop,
                UIActivityTypeCopyToPasteboard,
                UIActivityTypePrint,
                UIActivityTypeAddToReadingList]
        
        self.presentViewController(activityView, animated: true, completion: nil)
    }
}
