//
//  CoreDataHelper.swift
//  apupyshevIMDBmkTest
//
//  Created by Alexey Pupyshev on 01/09/15.
//  Copyright (c) 2015 Alexey Pupyshev. All rights reserved.
//

import UIKit
import CoreData

class CoreDataHelper: NSObject, NSFetchedResultsControllerDelegate {
    class var sharedInstance: CoreDataHelper {
        struct Singleton {
            static let sharedInstance = CoreDataHelper()
        }
        
        return Singleton.sharedInstance
    }
    
    let coordinator: NSPersistentStoreCoordinator
    let model: NSManagedObjectModel
    let context: NSManagedObjectContext
    
    private override init() {
        let modelURL = NSBundle.mainBundle().URLForResource("apupyshevIMDBmkTest", withExtension: "momd")!
        model = NSManagedObjectModel(contentsOfURL: modelURL)!
        
        let fileManager = NSFileManager.defaultManager()
        let docsURL = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).last as! NSURL
        let storeURL = docsURL.URLByAppendingPathComponent("mTestBase.sqlite")
        
        coordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
        
        let store = coordinator.addPersistentStoreWithType(
            NSSQLiteStoreType,
            configuration: nil,
            URL: storeURL,
            options: nil,
            error: nil)
        if store == nil {
            println("Store is empty")
        }
        
        context = NSManagedObjectContext()
        context.persistentStoreCoordinator = coordinator
        
        super.init()
    }
    
    func saveContext() {
        var error: NSError?
        if !context.save(&error) {
            println("Error with saving - \(error?.localizedDescription)")
        }
        
        println("Saved")
    }
    
    func getBookmarks() -> [Film] {
        let request = NSFetchRequest(entityName: "BookmarkedFilm")
        let sort = NSSortDescriptor(key: "imdbRating", ascending: false)
        let sortArray = NSArray(objects: sort)
        request.sortDescriptors = sortArray as [AnyObject]
        let list = CoreDataHelper.sharedInstance.context.executeFetchRequest(request, error: nil) as! [NSManagedObject]
        var films = [Film]()
        
        if list.count > 0 {
            for index in 0...list.count - 1 {
                var film: Film = Film()
                var managedObject = list[index] as NSManagedObject
                
                film.title = managedObject.valueForKey("title") as! String
                film.year = managedObject.valueForKey("year") as! String
                film.released = managedObject.valueForKey("released") as! String
                film.runtime = managedObject.valueForKey("runtime") as! String
                film.genre = managedObject.valueForKey("genre") as! String
                film.director = managedObject.valueForKey("director") as! String
                film.writer = managedObject.valueForKey("writer") as! String
                film.plot = managedObject.valueForKey("plot") as! String
                film.type = managedObject.valueForKey("type") as! String
                film.poster = managedObject.valueForKey("poster") as! NSData
                film.imdbRating = managedObject.valueForKey("imdbRating") as! String
                film.imdbID = managedObject.valueForKey("imdbID") as! String
                film.addedDate = managedObject.valueForKey("addedDate") as! String
                film.managedFilmID = managedObject.objectID
                
                films.append(film)
            }
        }
        
        return films
    }
    
    func clearStore() {
        var request = NSFetchRequest(entityName: "BookmarkedFilm")
        var List = context.executeFetchRequest(request, error: nil) as! [NSManagedObject]
        
        var bas: NSManagedObject!
        
        for obj: AnyObject in List {
            context.deleteObject(obj as! NSManagedObject)
        }
        
        println("Cleared")
    }
}
