//
//  BookmarkedResultsTableViewController.swift
//  apupyshevIMDBmkTest
//
//  Created by Alexey Pupyshev on 02/09/15.
//  Copyright (c) 2015 Alexey Pupyshev. All rights reserved.
//

import UIKit
import CoreData

class BookmarkedResultsTableViewController: UITableViewController {

    var bookmarkedFilms = [Film]()
    var rControl: UIRefreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "refresh:", name: "refreshTableView", object: nil)
        
        bookmarkedFilms = CoreDataHelper.sharedInstance.getBookmarks()
        
        self.tableView.reloadData()
        refreshControlInit()
    }

    func refresh(note: NSNotification) {
        bookmarkedFilms = CoreDataHelper.sharedInstance.getBookmarks()
        
        self.tableView.reloadData()
        self.rControl.endRefreshing()
    }
    
    // MARK: - TableViewDelegate methods

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if bookmarkedFilms.count == 0 {
            var messageLabel = UILabel(frame: CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height))
            
            messageLabel.text = "No data is currently available.";
            messageLabel.textColor = UIColor.blackColor()
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = NSTextAlignment.Center
            messageLabel.font = UIFont(name: "Helvetica", size: 20)
            
            self.tableView.backgroundView = messageLabel;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None;
            
            return 0;
        } else {
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
        }
        
        return bookmarkedFilms.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! UITableViewCell

        var film = bookmarkedFilms[indexPath.row] as Film
        
        cell.textLabel!.text = film.title
        cell.detailTextLabel!.text = "Date: " + film.addedDate
        
        return cell
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "detailBookmarkedFilmSegue" {
            let vc = segue.destinationViewController as! FilmDetailViewController
            vc.flag = 1
            vc.film = bookmarkedFilms[self.tableView.indexPathForSelectedRow()!.row] as Film
        }
    }
    
    func refreshControlInit() {
        self.rControl = UIRefreshControl()
        self.rControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(rControl)
    }
}
