//
//  Film.swift
//  apupyshevIMDBmkTest
//
//  Created by Alexey Pupyshev on 02/09/15.
//  Copyright (c) 2015 Alexey Pupyshev. All rights reserved.
//

import Foundation
import CoreData

class Film : NSObject {
    var title: String = ""
    var year: String = ""
    var released: String = ""
    var runtime: String = ""
    var genre: String = ""
    var director: String = ""
    var writer: String = ""
    var plot: String = ""
    var type: String = ""
    var poster: NSData = NSData()
    var imdbRating: String = ""
    var imdbID: String = ""
    var addedDate: String = ""
    var managedFilmID: NSManagedObjectID = NSManagedObjectID()
    
    func saveManaged() {
        let entity = NSEntityDescription.entityForName("BookmarkedFilm", inManagedObjectContext: CoreDataHelper.sharedInstance.context)
        let filmM = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: CoreDataHelper.sharedInstance.context) as NSManagedObject
        
        filmM.setValue(title, forKey: "title")
        filmM.setValue(year, forKey: "year")
        filmM.setValue(released, forKey: "released")
        filmM.setValue(runtime, forKey: "runtime")
        filmM.setValue(genre, forKey: "genre")
        filmM.setValue(director, forKey: "director")
        filmM.setValue(writer, forKey: "writer")
        filmM.setValue(plot, forKey: "plot")
        filmM.setValue(type, forKey: "type")
        filmM.setValue(poster, forKey: "poster")
        filmM.setValue(imdbRating, forKey: "imdbRating")
        filmM.setValue(imdbID, forKey: "imdbID")
        filmM.setValue(addedDate, forKey: "addedDate")
    
        CoreDataHelper.sharedInstance.saveContext()
    }
    
    func removeManaged() {
        let request = NSFetchRequest(entityName: "BookmarkedFilm")
        let list = CoreDataHelper.sharedInstance.context.executeFetchRequest(request, error: nil) as! [NSManagedObject]
        
        if list.count > 0 {
            for i in 0...list.count - 1 {
                var moc = list[i] as NSManagedObject
                
                if moc.objectID == managedFilmID {
                    CoreDataHelper.sharedInstance.context.deleteObject(moc)
                    break;
                }
            }
        }
    
        CoreDataHelper.sharedInstance.saveContext()
    }
}