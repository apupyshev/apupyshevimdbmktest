//
//  APIAssistent.swift
//  apupyshevIMDBmkTest
//
//  Created by Alexey Pupyshev on 02/09/15.
//  Copyright (c) 2015 Alexey Pupyshev. All rights reserved.
//

import Foundation

struct queryDetail {
    static var url = "http://www.omdbapi.com/"
    static var infoPart = "?i="
    static var srchPart = "?s="
    static var jsonPart = "&y=&plot=short&r=json"
}

class APIAssistent : NSObject {
    
    class func getFilmByID(id: String) -> Film {
        var film = Film()
        var error: NSError?
        
        if let data = NSData(contentsOfURL: NSURL(string: queryDetail.url + queryDetail.infoPart + id + queryDetail.jsonPart)!, options: nil, error: &error) {
            var json = JSON(data: data as NSData)
            
            film.title = json["Title"].stringValue
            film.year = json["Year"].stringValue
            film.released = json["Released"].stringValue
            film.runtime = json["Runtime"].stringValue
            film.genre = json["Genre"].stringValue
            film.director = json["Director"].stringValue
            film.writer = json["Writer"].stringValue
            film.plot = json["Plot"].stringValue
            film.type = json["Type"].stringValue
            
            let poster = json["Poster"].stringValue
            
            if poster != "N/A" {
                if let data = NSData(contentsOfURL: NSURL(string: poster)!, options: nil, error: &error) {
                    film.poster = data
                } else {
                    println("Error in 'getPosterFromFilm' is - \(error?.localizedDescription)")
                }
            } else {
                film.poster = NSData()
            }
            
            film.imdbRating = json["imdbRating"].stringValue
            film.imdbID = id
            
        } else {
            println("Error in 'getFilmByID' is - \(error?.localizedDescription)")
        }
        
        return film
    }
    
    class func searchFilmsByRequest(request: String) -> [Film] {
        var rightRequest: String = request.stringByReplacingOccurrencesOfString(" ", withString: "+")
        var films = [Film]()
        var error: NSError?
        
        if let data = NSData(contentsOfURL: NSURL(string: queryDetail.url + queryDetail.srchPart + rightRequest + queryDetail.jsonPart)!, options: nil, error: &error) {
            var json = JSON(data: data as NSData)
            
            if json["Search"].count > 0 {
                for film in 0...json["Search"].count - 1 {
                    if(json["Search"][film]["Type"] == "movie") {
                        var film: Film = getFilmByID(json["Search"][film]["imdbID"].stringValue) as Film
                        
                        if film.plot != "N/A" {
                            films.append(film)
                        }
                    }
                }
            } else {
                return films
            }
        } else {
            println("Error in 'searchFilmsByRequest' is - \(error?.localizedDescription)")
        }
        
        return films
    }
}
