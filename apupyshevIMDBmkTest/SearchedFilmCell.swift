//
//  SearchedFilmCell.swift
//  apupyshevIMDBmkTest
//
//  Created by Alexey Pupyshev on 02/09/15.
//  Copyright (c) 2015 Alexey Pupyshev. All rights reserved.
//

import UIKit

class SearchedFilmCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var posterView: UIImageView!
    
}
