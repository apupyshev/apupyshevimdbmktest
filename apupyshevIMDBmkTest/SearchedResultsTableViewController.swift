//
//  SearchedResultsTableViewController.swift
//  apupyshevIMDBmkTest
//
//  Created by Alexey Pupyshev on 02/09/15.
//  Copyright (c) 2015 Alexey Pupyshev. All rights reserved.
//

import UIKit

class SearchedResultsTableViewController: UITableViewController, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var films = [Film]()
    var messageLabel = UILabel()
    var activity = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Making cell resizeble
        self.tableView.estimatedRowHeight = 44.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.searchBar.delegate = self
        
        activityInit()
        messageLabelInit()
    }
    
    // MARK: - TableViewDelegate methods
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if films.count == 0 {
            self.tableView.backgroundView = messageLabel
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None;
            
            return 0;
        } else {
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
        }
        
        return films.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: SearchedFilmCell = tableView.dequeueReusableCellWithIdentifier("SearchedFilmCell", forIndexPath: indexPath) as! SearchedFilmCell
        
        var film: Film = films[indexPath.row] as Film
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None // No selection style
        cell.titleLabel.text = film.title
        cell.descriptionLabel.text = film.plot
        cell.descriptionLabel.layoutIfNeeded() // Auto size for description label
        cell.posterView.image = UIImage(data: film.poster)
        cell.posterView.layoutIfNeeded() // Auto size for poster
        
        return cell
    }
    
    // MARK: - SearchBarDelegate method
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        self.view.addSubview(activity)
    
        self.tableView.backgroundView = nil
        
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_UTILITY.value), 0)) {
            let results = APIAssistent.searchFilmsByRequest(searchBar.text)
            
            dispatch_async(dispatch_get_main_queue()) {
                
                self.films = results
                self.tableView.reloadData()
                self.activity.removeFromSuperview()
            }
        }
        
        self.tableView.reloadData()
    }
    
    // MARK: - Prepare for segue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "detailSearchedFilmSegue") {
            let vc = segue.destinationViewController as! FilmDetailViewController
            var indexPath = self.tableView.indexPathForSelectedRow()!
            vc.film = films[indexPath.row] as Film
            vc.flag = 0
        }
    }
    
    // MARK: - ActivityIndicator implementation
    
    func activityInit() {
        activity = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        activity.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.White
        activity.center = CGPointMake(self.view.center.x, self.view.center.y - 60)
        activity.backgroundColor = UIColor(white: 0.3, alpha: 0.8)
        activity.layer.cornerRadius = 10
        activity.startAnimating()
    }
    
    func messageLabelInit() {
        self.messageLabel = UILabel(frame: CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height))
        
        self.messageLabel.text = "No results.";
        self.messageLabel.textColor = UIColor.blackColor()
        self.messageLabel.numberOfLines = 0
        self.messageLabel.textAlignment = NSTextAlignment.Center
        self.messageLabel.font = UIFont(name: "Helvetica", size: 20)
        
        self.tableView.backgroundView?.addSubview(messageLabel)
    }
}
