//
//  SaveToCoreDataActivity.swift
//  apupyshevIMDBmkTest
//
//  Created by Alexey Pupyshev on 02/09/15.
//  Copyright (c) 2015 Alexey Pupyshev. All rights reserved.
//

import UIKit

class SaveToCoreDataActivity: UIActivity {
    
    var customActivityType = ""
    var activityName = ""
    var customActionWhenTapped:( (Void)-> Void)!
    
    init(title: String, performAction: (() -> ()) ) {
        self.activityName = title
        self.customActivityType = "Action \(title)"
        self.customActionWhenTapped = performAction
        super.init()
    }
    
    override func activityType() -> String? {
        return customActivityType
    }
    
    override func activityTitle() -> String? {
        return activityName
    }
    
    override func activityImage() -> UIImage? {
        // FIX IMAGE
        var image = UIImage(named: "addToBookmarks_iOS8")!
        image.resizingMode
        
        return image
    }
    
    override func canPerformWithActivityItems(activityItems: [AnyObject]) -> Bool {
        return true
    }
    
    override func prepareWithActivityItems(activityItems: [AnyObject]) {
        // nothing to prepare
    }
    
    override func activityViewController() -> UIViewController? {
        return nil
    }
    
    override func performActivity() {
        customActionWhenTapped()
    }
}